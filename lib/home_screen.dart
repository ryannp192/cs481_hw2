import 'dart:ui';

import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget> [
          Container(
            height: size.height * .3,
            decoration: BoxDecoration(
              image: DecorationImage(
                alignment: Alignment.topCenter,
                image: AssetImage('images/maxresdefault.jpg')
              )
            ),
          ),

          Align(
            alignment: Alignment (0.0, -0.25),

           child:
             Padding(
               padding: const EdgeInsets.all(16.0),
               child: Text('osu! is not Oregon Statue University. It is distinguished by its full use of lowercase letters and an exclamation mark at the end.'
                   ' osu! a Rhythm game with extreme depth and precision. '
                   'In this game you click circles to the beat of a song. It may sound very easy but in reality there is no '
                   'limit to how far the game can be pushed in terms of difficulty. There are multiple modes to play as well, such as: Taiko!, Catch!, and Mania! I couldnt figure out how to make the icons appear :( ', style: TextStyle(
                 fontWeight: FontWeight.bold,)
            ),
             ),
         ),
          Align(
            alignment: Alignment( 0.0, 0.15),

           child:
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text('Now lets see you click some circles!', style: TextStyle(
                  fontSize: 30.0,
                    shadows: [
                  Shadow(
                    color: Colors.blue,
                    blurRadius: 10.0,
                    offset: Offset(5.0, 5.0)
                ),
                 Shadow(
                   color: Colors.red,
                   blurRadius: 10.0,
                   offset: Offset(-5.0, 5.0),
                 ),
                    ],

                ),
                  ),

              ),
          ),
           ],
         ),
              );
  }






}