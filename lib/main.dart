import 'package:flutter/material.dart';


import 'package:layout_hw/home_screen.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}




double _volume = 0.0;

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.volume_up, size: 100.0, color: Colors.pinkAccent,),
          tooltip: 'Increase volume by 10',
          onPressed: () {
            setState(() {
              _volume += 10;
            });
          },
        ),
        Text('Volume : $_volume')
      ],
    );
  }
}








/*
class CircleWidget extends StatefulWidget {
 @override
  _CircleWidgetState createState() => _CircleWidgetState();
}
int _clicks = 0;
class _CircleWidgetState extends State<CircleWidget> {
  bool _isClicked = true;
  int _clickCount = 50;

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.add_circle),
            tooltip: 'Increase Click by 1',
            onPressed: () {
              setState(() {
                _clicks += 1;
              });
            },
          ),
          Text('Clicks: $_clicks')
        ],
    );
  }
}

*/

